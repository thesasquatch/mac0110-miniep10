function max_sum_submatrix(matrix)
    siz = size(matrix)
    m = siz[1]
    d = []
    unit = []
    unit2 = []
    piv = matrix[1, 1]
    push!(unit, matrix[1, 1])
    n = 2
    for i in eachindex(matrix)
        if matrix[i] > piv
            piv = matrix[i]
            empty!(unit)
            push!(unit, matrix[i])
        elseif matrix[i] == piv
            push!(unit, matrix[i])
        end
    end
    if piv == matrix[1, 1]
        deleteat!(unit, 1)
    end
    for i in 2:m
        for j in i:m
            for c in i:m
                if sum(matrix[(j-(i-1)):j,(c-(i-1)):c]) > piv 
                    piv = sum(matrix[(j-(i-1)):j,(c-(i-1)):c])
                    empty!(d)
                    push!(d, matrix[(j-(i-1)):j,(c-(i-1)):c])
                elseif sum(matrix[(j-(i-1)):j,(c-(i-1)):c]) == piv
                    push!(d, matrix[(j-(i-1)):j,(c-(i-1)):c])
                end
            end
        end    
    end
    if d == []
        return unit
    else
        return d
    end
end
    
